---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: resume
---

Since November 2024, I have been working at [Ignition Computing](https://ignitioncomputing.com/), a start-up in Eindhoven focused on developing software that powers science. 
In my role, I design and develop a data-driven linear solver library that accelerates physics simulations.
Working at a small company, I tackle a wide variety of challenges, from sales and strategy to software development and complex physics/mathematical problems.
The dynamic, versatile nature of the job and the opportunity to contribute to the company in many different ways strongly motivate me.